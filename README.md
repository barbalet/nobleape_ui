Some music from the development (also available via Apple Music, YouTube and Spotify):

[Noble Ape: Last Monkey Standing](https://www.amazon.com/Noble-Ape-Last-Monkey-Standing/dp/B07JFQC4GQ/)

[Noble Ape: Monkey Freedom](https://www.amazon.com/Noble-Ape-Freedom-Tom-Barbalet/dp/B07M9389KQ/)

[Noble Ape: Milk and Medicine](https://www.amazon.com/Noble-Ape-Medicine-Tom-Barbalet/dp/B07T14QGVP/)

[![Noble Ape on Emergence](http://i.ytimg.com/vi/BdANrTDer9c/hqdefault.jpg)](//www.youtube.com/watch?v=BdANrTDer9c "Noble Ape on Emergence")

## DESCRIPTION

The Noble Ape Simulation has been in development since 1996. It features a number of autonomous simulation components including a landscape simulation, biological simulation, weather simulation, sentient creature (Noble Ape) simulation (including a number of internal biological simulations) and a simple intelligent-agent scripting language (ApeScript).

[![Noble Ape - I've Been Working On It Since 1996...](http://i.ytimg.com/vi/xmieOO623wY/hqdefault.jpg)](//www.youtube.com/watch?v=xmieOO623wY "Noble Ape - I've Been Working On It Since 1996...")

## DETAILS and SUPPORT

[Noble Ape main page](http://www.nobleape.com/).

[Noble Ape Simulation main page](http://www.nobleape.com/sim/).

[![Noble Ape Philosophic](http://i.ytimg.com/vi/zXDenS8mO70/hqdefault.jpg)](//www.youtube.com/watch?v=zXDenS8mO70 "Noble Ape Philosophic")

[Noble Ape Simulation Manual](http://www.nobleape.com/man/).

[Noble Ape Simulation developer mailing list](http://www.nobleape.com/mailman/listinfo/developer_nobleape.com).

Build instructions can be found in /sim/documentation/build.html

[![Why Is Noble Ape Free?](http://i.ytimg.com/vi/M8-BrQTYV20/hqdefault.jpg)](//www.youtube.com/watch?v=M8-BrQTYV20 "Why Is Noble Ape Free?")

## LICENSE

http://www.nobleape.com/man/legal.html

Noble Ape Open Source License

Copyright Tom Barbalet, 1996-2019. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software is a continuing work of Tom Barbalet, begun on 13 June 1996. No apes or cats were harmed in the writing of this software.
